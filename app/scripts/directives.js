angular.module('angularApp')
.directive('personCard', function() {
	return {
		restrict:'EAC',
		replace: true,
		scope:{
			data:'=person', // two-way bind
			title:'@?', // optional string
			click:'&myClick'
		},
		template:'<div ng-click="click()" \
			style="border:1px solid gray; padding: 10px;">\
			<h4 ng-bind="title || \'Person Card\'"></h4>\
			<br/>\
			<input ng-model="data.firstName">\
		</div>'
	}
})
.directive('domDirective', function() {
	return {
		link: function(scope, $element, attrs){

			$element.html('Hello World!');

			$element.on('click', function(){
				// apply changes to scope within $digest phase
				scope.$apply(function(){
					scope.isolated = 'Hello'
					//window.alert('Hello!')
				})
			})
		}
	}
})
.directive('barcode', function(){
	return {
		scope:{
			code:'=barcode',
			type:'=?'
		},
		template:'<div>\
			<figure>\
				<div class="barcode"></div>\
				<figcaption ng-bind="code"></figcaption>\
				<select class="type">\
					<option value="ean8">ean8</option>\
					<option value="code11">code11</option>\
				</select>\
			</figure>\
		</div>',
		link: function(scope, $element, attrs, controller){

			var code_img = $element.find('.barcode');
			var type_sel = $element.find('.type');

			if(!scope.type){
				scope.type = 'ean8'
			}
			function render(){
				code_img.barcode(scope.code,scope.type);
			}

			// make angular $watch scope and update 3rd party code
			scope.$watch('code',function(newVal,oldVal){
				if(!newVal){
					return;
				}
				render();
			});

			scope.$watch('type',function(newVal,oldVal){
				/*if(newVal===oldVal){
					return;
				}*/
				type_sel.val(newVal);
				render();
			});

			// Change in 3rd party code $apply changes to angular scope
			type_sel.on('change', function(event){
				scope.$apply(function(){
					scope.type = type_sel.val();
				})
			})

			$scope.$on('$destroy', function(){
				//remove  jquery event handlers
				type_sel.off('change');
			})

			// You can use Zone.JS with angular 1.5(google it)
			// for automatic change detection even in 3rd party code
		},
		controller: function(){


		},
		controllerAs: 'ctrl',
	}
})
.directive('transcludeDemo',function(){
	return {
		transclude:true,
		template:'<div style="border:1px solid black">\
		 <h3> Transcluded </h3>\
			<div class=".my-transcluded-body">\
			\
				<ng-transclude>Default Content</ng-transclude>\
			\
			</div>\
		<div>'
	}
})
.component('homework-component',{
	binding: function(){

	},
	controller: function(){

	}
});