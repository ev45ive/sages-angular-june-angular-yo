angular
.module('angularApp')
.controller('EmployeesCtrl', EmployeesCtrl);

/*@ngInject*/
function EmployeesCtrl($scope, EmployeesService) {


	this.allEmployees = EmployeesService.getAll();

	this.list = [];

	this.setManager = function(manager){
		this.manager = manager;

		/*this.list = this.allEmployees.filter(function(employee){
			return manager.firstName == employee.manager;
		})*/
	}	

	this.addEmployee = function(employee, manager){
		this.allEmployees.push({
			firstName: employee.firstName,
			lastName: employee.lastName,
			manager: manager.firstName
		});
		//this.setManager(this.manager);
	}
}