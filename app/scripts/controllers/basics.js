angular.module('angularApp')
.controller('BasicsCtrl', function($scope){

	var user = {
		name: 'Zenobiusz',
		age: 32,
		sayHello: function(){
			$scope.greeting = 'Hello, I am'+user.name;
		}
	}

	$scope.user = user;
})
.controller('SubBasicsCtrl', function($scope){
	
})