angular
.module('angularApp')
.controller('PersonCtrl', PersonCtrl);

function PersonCtrl(){

	this.firstName = 'Zenon';
	this.lastName = 'Testowy';
}
PersonCtrl.prototype.getFullName = function(){
	return this.firstName + ' ' + this.lastName
}
PersonCtrl.prototype.clear = function(){
	this.firstName = this.lastName = '';
}