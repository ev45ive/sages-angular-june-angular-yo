angular.module('angularApp')
.provider('DataService', function(){
	
	var server_url='';

	function handeAPIError(err){
		console.error(err)
	}

	return {
		setServerURL: function(url){
			server_url = url;
		},
		// Data Service Factory
		$get: function($http){

			function Resource(name,id,data){
				this.name = name;
				this.id = id;
				this.data = data || {}
			}
			Resource.prototype.get = function(){

			    return $http.get(this.buildURL())
			    .then( this.handleResponse.bind(this) )
			    .then(function(data){
			    	return this.set(data)
			    }.bind(this))
				.catch(handeAPIError)
			}
			Resource.prototype.set = function(data){
				return this.data = data;
			}
			Resource.prototype.create = function(){
			    return $http.post(this.buildURL())
			    .then( this.handleResponse.bind(this) )
				.catch(handeAPIError)
			}
			Resource.prototype.save = function(){
				var method = 'post';
				// if object exists (has an id)
				if(this.data && this.data.id){
					method = 'put';
				}
			    return $http({
			    	url: this.buildURL(),
			    	method: method,
			    	data: this.data
		    	})
			    .then( this.handleResponse.bind(this) )
				.catch(handeAPIError)
			}
			Resource.prototype.buildURL = function(){
				return server_url + [this.name,this.id].join('/');
			}
			Resource.prototype.handleResponse = function(response){
				this.lastResponse = response;

				return response.data;
			}

			return {
				getResource: function(name, id){
					return new Resource(name, id);
				}
			}
		}
	}
})