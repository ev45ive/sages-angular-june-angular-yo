'use strict';

/**
 * @ngdoc overview
 * @name angularApp
 * @description
 * # angularApp
 *
 * Main module of the application.
 */
angular
  .module('angularApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    //'ngRoute',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ]);


angular
  .module('angularApp')

  // availavble only after alls .config() are done
  .value('session',{ 
    token: ''
  })
  // available in config
  .constant('config',{
    server_url: 'http://192.168.1.13:3000/'
  })

  // Configure Providers before anything is run
  .config(function(config, DataServiceProvider){

    DataServiceProvider.setServerURL(config.server_url);
  })

  // Work with services 
  .run(function(SessionService,DataService, config){

    var TokenResource = DataService.getResource('tokens','1')

    SessionService.setResource(TokenResource);
    SessionService.requestToken();

  })

  .run(function($rootScope, $state){
    $rootScope.$on('$stateChangeError',function(){
      console.log('State change  Error',$state);
      $state.go('main')
    })
  })


  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      // example of abstract states
      .state('users',{
        abstract: true,
        template: '<h1>Users</h1> <ui-view></ui-view>',
        url: '/users',
        resolve: {
          // When usersResource is created, fetch users
          users: function(usersResource){
            return usersResource.get();
          },
          // get users resource
          usersResource: function(DataService){
            return DataService.getResource('users');
          },
        }
      })
        .state('users.list',{
          // Abstract state in url matches this one by default
          url:'',
          templateUrl: 'views/users.html',
          controller: function($scope,usersResource){
            $scope.users = usersResource;
          },
        })
        .state('users.add',{
          url:'/add',
          templateUrl: 'views/add_user_form.html',
          controller: function($scope, userResource){
            $scope.user = userResource.data;

            $scope.save = function(userData){
              userResource.set(userData);
              userResource.save();
            }
          },
          resolve: {
            // get users resource
            userResource: function(DataService){
              return DataService.getResource('users');
            },
          }
        })
      // Mounting Controller on whole $scope
      .state('basics',{
        url: '/basics',
        templateUrl: 'views/basics.html',
        controller: 'BasicsCtrl'
      })
      // Mounting Controller as 'employee' $scope field
      .state('person',{
        url: '/person',
        templateUrl: 'views/person.html',
        controller: 'PersonCtrl',
        controllerAs: 'employee'
      })
      .state('directives',{
        abstract:true,
        url:'/directives',
        template: '<ui-view></ui-view/>'
      })
          .state('directives.simple',{
            url:'/simple',
            templateUrl:'views/directives-simple.html',
            controller: function($scope){
              $scope.isolated = 'Test';

              $scope.alert = function(person){

                debugger;
                window.alert(person.firstName)
              }

              this.firstName = 'Directing'
              this.lastName = 'Director'
            },
            controllerAs:'employee',
          })
          .state('directives.barcode',{
            url:'/barcode',
            templateUrl:'views/directives-barcode.html'
          })
      .state('manager',{
        url: '/manager',
        templateUrl: 'views/manager.html',
        controller: 'PersonCtrl',
        controllerAs:'manager',
      })
          .state('manager.employees',{
            url: '/employees',
            templateUrl: 'views/manager-employees.html',
            controller: 'EmployeesCtrl',
            controllerAs:'employees',
          })
          .state('manager.employees.show_employee',{
            url:'/employee/:firstName',
            templateUrl: 'views/edit-employee.html',
            // Resolve data and async dependencies based on url parameters ($stateParams)
            resolve:{
              employees: function($stateParams,EmployeesService){
                return EmployeesService.getAll().filter(function(emp){
                  return emp.firstName === $stateParams.firstName;
                });
              }
            },
            // Inject resollved local values into controller
            controller: function(employees){
              this.firstName = employees[0].firstName;
            },
            controllerAs:'employee',
          })
          .state('manager.employees.add_employee',{
            url:'/employee',
            templateUrl: 'views/manager-employee.html',
            controller: 'PersonCtrl',
            controllerAs:'employee',
          })
      .state('about',{
        url: '/about',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      });

      //  If none matches URL, use '/'
      $urlRouterProvider.otherwise('/');
  });

// Bootstrap Angular - Start Parser from <body>
// using definitions form 'angularApp' module
$(document).ready(function(){
  angular.bootstrap(document.body,['angularApp']);
})