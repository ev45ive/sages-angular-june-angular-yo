angular.module('angularApp')
.factory('SessionService', function($http){
	
	var session = {
		token:''
	}
	var resource = null;

	var Storage = localStorage;

	var api = {
		setResource: setResource,
		getToken: getToken,
		setToken: setToken,
		requestToken: requestToken,
		invalidateToken: invalidateToken, 
	}
	return api;

	// ===========================

	function setResource(res){
		return resource = res;
	}

	function getToken(){
		return session.token;
	}

	function setToken(token){
		return session = Object.assign({},session,token);
	}

	function requestToken(){
		return resource.get()

		// handle success
		.then(setToken)

		// Use Storage API
		.then(saveSession)
	}

	function invalidateToken(){
		return session.token = '';
	}

	// Storage API:
	function saveSession(){
		Storage.setItem('session',JSON.stringify(session))
	}
	function loadSession(){
		JSON.parse(Storage.getItem('session'))
	}
	function removeSession(){
		Storage.removeItem('session')
	}

})