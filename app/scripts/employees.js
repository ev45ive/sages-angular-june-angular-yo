angular.module('angularApp')
.factory('EmployeesService', function EmployeesService($http){
	
	var allEmployees = [
		{firstName:'Zenon', manager:''},
		{firstName:'Stachu', manager:'Zenon'},
		{firstName:'Zbychu', manager:'Zenon'}
	]

	return {
		getAll: function(){
			return allEmployees;
		}
	}


})